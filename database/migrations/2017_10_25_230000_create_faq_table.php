<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFaqTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('faq', function (Blueprint $table) {
            $table->increments('id');
            $table->string('short_description');
            $table->string('description');
            $table->integer('type_id')->unsigned();
            $table->integer('author_id')->unsigned();
            $table->timestamps();

            //$table->foreign('type_id')->references('id')->on('faq_type');
            //$table->foreign('author_id')->references('id')->on('users');
        });

        Schema::table('faq', function($table) {
            $table->foreign('author_id')->references('id')->on('users');
            $table->foreign('type_id')->references('id')->on('faq_type');

            //$table->foreign('type_id')->references('id')->on('faq_type');
            //$table->foreign('author_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('faq');
    }
}
