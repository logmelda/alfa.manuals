<?php

use Illuminate\Database\Seeder;

class FaqTypesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('faq_type')->insert([
            'description' => 'Crm'
        ]);

        DB::table('faq_type')->insert([
            'description' => 'Outlook'
        ]);

        DB::table('faq_type')->insert([
            'description' => 'Email'
        ]);

        DB::table('faq_type')->insert([
            'description' => 'Impressoras'
        ]);

        DB::table('faq_type')->insert([
            'description' => 'Suporte'
        ]);
    }
}
