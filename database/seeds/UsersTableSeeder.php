<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'username' => 'rbs',
            'name' => 'Ricardo Santos',
            'email' => 'ricardo.santos@alfaloc.pt',
            'password' => bcrypt('kanguru11'),
            'role' => 'A'
        ]);

        DB::table('users')->insert([
            'username' => 'dn',
            'name' => 'Daniel Neto',
            'email' => 'daniel.neto@alfaloc.pt',
            'password' => bcrypt('kanguru11'),
            'role' => 'A'
        ]);
    }
}
