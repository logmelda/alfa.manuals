<?php

use Illuminate\Database\Seeder;

class FaqsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('faq')->insert([
            'short_description' => 'Configurar Regras no Outlook',
            'description' => '...',
            'type_id' => '2',
            'author_id' => '2'
        ]);

        DB::table('faq')->insert([
            'short_description' => 'Configurar Conta de Email no Outlook',
            'description' => '...',
            'type_id' => '2',
            'author_id' => '1'
        ]);

        DB::table('faq')->insert([
            'short_description' => 'Agendar Visita no CRM',
            'description' => '...',
            'type_id' => '1',
            'author_id' => '1'
        ]);

        DB::table('faq')->insert([
            'short_description' => 'Criar Ticket para o Suporte',
            'description' => '...',
            'type_id' => '5',
            'author_id' => '2'
        ]);
    }
}
