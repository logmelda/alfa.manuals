<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Faq;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $faqs = Faq::all();


        return view('home', [
            'faqs' => $faqs
        ]);
    }
}
